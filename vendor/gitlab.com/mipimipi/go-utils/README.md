[![GoDoc](https://godoc.org/gitlab.com/mipimipi/go-utils?status.svg)](https://godoc.org/gitlab.com/mipimipi/go-utils)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/mipimipi/go-utils)](https://goreportcard.com/report/gitlab.com/mipimipi/go-utils)
[![REUSE status](https://api.reuse.software/badge/gitlab.com/mipimipi/go-utils)](https://api.reuse.software/info/gitlab.com/mipimipi/go-utils)

# go-utils (Go Utilities)

Practical and handy functions that are useful in many Go projects, but which are not part of the standards Go libraries.

Sub package [file](https://godoc.org/gitlab.com/mipimipi/go-utils/file) contains functions that help dealing with files and directories, sub package [workerpool](https://godoc.org/gitlab.com/mipimipi/go-utils/workerpool) supports executing many tasks/calculations concurrently.

Detailed documention is on [godoc.org](https://godoc.org/gitlab.com/mipimipi/go-utils). 

## Installation

Install `go-utils` via the `go` tool by executing

    go get gitlab.com/mipimipi/go-utils

## License

[GNU Public License v3.0](https://gitlab.com/mipimipi/go-utils/blob/master/LICENSE)
