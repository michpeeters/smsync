module gitlab.com/mipimipi/smsync

go 1.14

require (
	github.com/eiannone/keyboard v0.0.0-20190314115158-7169d0afeb4f
	github.com/logrusorgru/aurora v0.0.0-20200102142835-e9ef32dff381 // indirect
	github.com/ricochet2200/go-disk-usage v0.0.0-20150921141558-f0d1b743428f
	github.com/sirupsen/logrus v1.5.0
	github.com/spf13/cobra v1.0.0
	gitlab.com/mipimipi/go-utils v0.0.0-20200415050052-bb00aca14cd0
	gopkg.in/yaml.v2 v2.2.8
)
